# Dependency options

option('microdns',
    type : 'feature',
    value : 'auto',
    description : 'mDNS discovery support using libmicrodns')
